const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id : user._id,
		email : user.email,
		isAdmin: user.Admin

	}

								// callback function for time limit
	return jwt.sign(data, secret, {})
};